$(document).ready(function(){
  $(".lazy").lazyload({
    threshold : 150,
    effect : "fadeIn"
  });
  $(".button-responsive").sideNav();
   $('.collapsible').collapsible();
  $('.section').scrollSpy({
    scrollOffset: 55
  });
  $(".dropdown-button").dropdown({
    hover: true,
    belowOrigin: true

  });
  $('.slider').slider({
    indicators: true
  });
  //headroom
  var header = document.querySelector("#header");
  new Headroom(header, {
    tolerance: {
      down : 1,
      up : 1
    },
    offset : 80,
    classes: {
      initial: "slide",
      pinned: "slide--reset",
      unpinned: "slide--up",
      top: "slide--top",
      notTop: "slide--notop"
    }
  }).init();

  var ancho = $(window).width();
  if(ancho > 600){

    /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
    particlesJS.load('particles-js', 'particles.json', function() {
      console.log('callback - particles.js config loaded');
    });
  }
  $('.fade').slick({
    // dots: true,
    // infinite: true,
    // speed: 400,
    // fade: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500
  });

  wow = new WOW({
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       false,       // default
    live:         true        // default
  })
  function starterAnim(){
      $("#fakebg").addClass('loadedb');
      setTimeout(function(){ $("#header").css({display: 'block', opacity: '1'});}, 550);
      setTimeout(function(){$("#fakebg").css("display", "none");},550);
      setTimeout(function(){$("body").css({overflow: 'auto',display:'none', opacity: '1'}).fadeTo("slow",1);},550)
      // setTimeout(function(){ $("body").css("overflow", "hidden");});
      thedelay = 550;
      $('.mascota').delay(thedelay).animate({opacity: '1', left: '0'}, 550);
      thedelay +=650;
      wow.init();
  }
 // var tover = ['01-G-250.png','02-U-250.png','03-I-250.png','04-A-250.png','05-S-250.png','06-D-250.png','07-E-250.png','08-B-250.png','08-O-250.png','09-L-250.png','10-I-250.png','11-V-250.png','12-I-250.png','13-A-250.png']; //just
 //  all = tover.length;
 //  var counter = 0;
 //  setTimeout(function(){ $("body").css("overflow", "hidden");});
 //  setTimeout(function(){ $("#header").css("display", "none");});
 //  for ( var i=0; i < all; i++){
 //    var img = new Image();
 //    img.onload = function(){
 //      counter+=1;
 //      console.log($(this).attr('src')+' - cargada!');
 //      console.log("inner counter"+counter);
 //      if (counter == all){
 //        starterAnim();
 //        console.log("now is all charged");
 //      }else{
 //      }
 //    }
 //    img.src="images/logo/"+tover[i]; //here we define the path
 //  }
 //  console.log("all"+all);
});


$(function(){
  $(window).on('scroll', function(){
    // var $scrollTop = $(window).scrollTop();
    // $('nav').text('El scroll es: ' + $scrollTop + 'px');
  })
})

jQuery(function($) {'use strict',
	/*Formulario de Mail*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
    $("label").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("../mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su Mensaje fue envíado correctamente");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        if (response == "llene"){
          message.html("Compruebe que no es un robot");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });
});
